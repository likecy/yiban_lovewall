
<?php

//易班介入授权文件；o.yiban.cn应用地址入口；校级权限；
ini_set("display_errors", "On");
error_reporting(0);

if (!function_exists('curl_init')) {
  throw new Exception('YiBan needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('YiBan needs the JSON PHP extension.');
}
if (!function_exists('mcrypt_decrypt')) {
  throw new Exception('YiBan needs the mcrypt PHP extension.');
}

//以下三个变量内容需换成本应用的
$APPID = "e3040ee72e630dc1";   //在open.yiban.cn管理中心的AppID
$APPSECRET = "72f888d98bfadec0fc306297854ca4af"; //在open.yiban.cn管理中心的AppSecret
$CALLBACK = "http://f.yiban.cn/iapp271544";  //在open.yiban.cn管理中心的oauth2.0回调地址

if(isset($_GET["code"])){   //用户授权后跳转回来会带上code参数，此处code非access_token，需调用接口转化。
    $getTokenApiUrl = "https://oauth.yiban.cn/token/info?code=".$_GET['code']."&client_id={$APPID}&client_secret={$APPSECRET}&redirect_uri={$CALLBACK}";
    $res = sendRequest($getTokenApiUrl);
    if(!$res){
        throw new Exception('Get Token Error');
    }
    $userTokenInfo = json_decode($res);
    $access_token = $userTokenInfo["access_token"];
}else{
    $postStr = pack("H*", $_GET["verify_request"]);
    if(strlen($APPID) == '16') {
        $postInfo = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $APPSECRET, $postStr, MCRYPT_MODE_CBC, $APPID);
    }else {
        $postInfo = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $APPSECRET, $postStr, MCRYPT_MODE_CBC, $APPID);
    }
    $postInfo = rtrim($postInfo);
    $postArr = json_decode($postInfo, true);
    if(!$postArr['visit_oauth']){  //说明该用户未授权需跳转至授权页面
        header("Location: https://openapi.yiban.cn/oauth/authorize?client_id={$APPID}&redirect_uri={$CALLBACK}&display=web");
        die;
    }
    $access_token = $postArr['visit_oauth']['access_token'];
  
    
}

//拿到access token了，接下来我们获取当前用户的基本信息试试看，so easy!
$userInfoJsonStr = sendRequest("https://openapi.yiban.cn/user/real_me?access_token={$access_token}");
$userInfoJsonStr = sendRequest("https://openapi.yiban.cn/user/real_me?access_token={$access_token}");
$userInfoJsonStr = sendRequest("https://openapi.yiban.cn/user/real_me?access_token={$access_token}");
$userInfoJsonStr = sendRequest("https://openapi.yiban.cn/user/real_me?access_token={$access_token}");


$userInfo_1 = json_decode($userInfoJsonStr,true);

$userInfo_school = sendRequest("https://openapi.yiban.cn/user/verify_me?access_token={$access_token}");
$userInfo_school = sendRequest("https://openapi.yiban.cn/user/verify_me?access_token={$access_token}");
$userInfo_school = sendRequest("https://openapi.yiban.cn/user/verify_me?access_token={$access_token}");
$userInfo_school = sendRequest("https://openapi.yiban.cn/user/verify_me?access_token={$access_token}");
$userInfo_2 = json_decode($userInfo_school,true);


session_start(); 
$_SESSION['pic'] =$userInfo_1['info']['yb_userhead'];//头像
$_SESSION['yb_realname'] = $userInfo_1['info']['yb_realname'];//真实姓名
$_SESSION['yb_collegename'] =$userInfo_2['info']['yb_collegename'];//学院
$_SESSION['yb_classname'] = $userInfo_2['info']['yb_classname'];//班级
$_SESSION['yb_studentid'] = $userInfo_2['info']['yb_studentid'];//学号

header('location:index.php');

function sendRequest($uri){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Yi OAuth2 v0.1');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array());
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    $response = curl_exec($ch);
    return $response;
}

?>