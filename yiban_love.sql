/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : yiban_love

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 10/12/2018 11:16:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for saylove_2017_blacklist
-- ----------------------------
DROP TABLE IF EXISTS `saylove_2017_blacklist`;
CREATE TABLE `saylove_2017_blacklist` (
  `id` int(11) NOT NULL,
  `ip` text NOT NULL,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for saylove_2017_commtents
-- ----------------------------
DROP TABLE IF EXISTS `saylove_2017_commtents`;
CREATE TABLE `saylove_2017_commtents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `contents` varchar(140) NOT NULL,
  `ip` text NOT NULL,
  `mtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=633 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for saylove_2017_guess
-- ----------------------------
DROP TABLE IF EXISTS `saylove_2017_guess`;
CREATE TABLE `saylove_2017_guess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `guessName` varchar(30) NOT NULL,
  `isRight` enum('0','1') NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=447 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for saylove_2017_like
-- ----------------------------
DROP TABLE IF EXISTS `saylove_2017_like`;
CREATE TABLE `saylove_2017_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `ip` text NOT NULL,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17639 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for saylove_2017_posts
-- ----------------------------
DROP TABLE IF EXISTS `saylove_2017_posts`;
CREATE TABLE `saylove_2017_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickName` varchar(30) NOT NULL,
  `school` text COMMENT '学院',
  `xuehao` text COMMENT '学号',
  `zuanye` text NOT NULL COMMENT '专业',
  `tureName` varchar(30) NOT NULL,
  `toWho` varchar(30) NOT NULL,
  `gender` enum('male','female','secrecy') NOT NULL,
  `itsGender` enum('male','female','secrecy') NOT NULL,
  `contents` longtext NOT NULL COMMENT '内容',
  `email` text NOT NULL,
  `isDisplay` enum('0','1') NOT NULL DEFAULT '0',
  `isSended` enum('0','1','2') NOT NULL DEFAULT '0',
  `love` int(11) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3445 DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
