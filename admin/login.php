<?php 
    session_start();

    // 账号密码验证标记，0为正确，1为错误
    $isWrong = 0;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        # 检测到提交的内容

        // 获取提交的账号和密码
        $username = test_input($_POST['username']);
        $password = test_input($_POST['password']);
        // 指定的正确的账号和密码
        $true_username = "账户";
        $true_password = "密码";

        // 比较
        if ($username == $true_username && $password == $true_password) {
            # 账号密码匹配
            $_SESSION['login'] = 1;
            # 使用脚本重定向回到登录界面
            $url="index.php";
            echo "<script language=\"javascript\">";
            echo "location.href=\"$url\"";
            echo "</script>";
            exit();
        } else {
            # 账号密码不匹配
            $_SESSION['login'] = 0;
            $isWrong = 1;
        }
    }

    // 对提交的数据进行处理和过滤
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>

<!DOCTYPE html>
<html>	
<head>
<title>绵师表白墙后台管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<meta name="keywords" content="Flat Dark Web Login Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--webfonts-->
<link href='http://fonts.useso.com/css?family=PT+Sans:400,700,400italic,700italic|Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.useso.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<script src="http://ajax.useso.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body>
<script>$(document).ready(function(c) {
	$('.close').on('click', function(c){
		$('.login-form').fadeOut('slow', function(c){
	  		$('.login-form').remove();
		});
	});	  
});
</script>
 <!--SIGN UP-->

<div class="login-form">
	<div class="close"> </div>
		<div class="head-info">
			<label class="lbl-1"> </label>
			<label class="lbl-2"> </label>
			<label class="lbl-3"> </label>
		</div>
			<div class="clear"> </div>
	<div class="avtar">
		<img src="images/avtar.png" />
	</div>
  
			<form action="?" method="POST">
        <div>
            
            <input type="text" name="username" value="">
        </div>
        <div>
          
            <input type="password" name="password" value="">
        </div>
      
	<div class="signin">
		 <input type="submit" name="submit" value="登录">
	</div>
       </form>
</div>
 <div class="copy-rights">
					<p>Copyright &copy; 2018.绵阳师范学院易班发展中心<br/>技术部 ---<a href="http://likecy.cn/" target="_blank">笔名无香</a></p>
			</div>
  
  
  
</body>
</html>