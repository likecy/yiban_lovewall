$(document).ready(function() {
  $("#submit").click(function(event) {
    var nickName = words_cut($("#nickName").val(), 30);
    var trueName = words_cut($("#trueName").val(), 30);
    var towho = words_cut($("#towho").val(), 30);
    var email = words_cut($("#email").val(), 30);
    var emailType = $("#emailType").val();
    var contents = $("#contents").val();
    var gender = $("#genderType").val();
    var itsGender = $("#itsGenderType").val();
    var Image = document.getElementById('image').files[0];
    if (email != "") {
      //拼接
      email += emailType;
    } else {
      email = "";
    }

     var formData = new FormData();

      formData.append('act','say');
      formData.append('nickName',nickName);
      formData.append('trueName',trueName);
      formData.append('towho',towho);
      formData.append('email',email);
      formData.append('contents',contents);
      formData.append('gender',gender);
      formData.append('itsGender',itsGender);
      formData.append('image',Image);

    if (nickName != "" && trueName != "" && towho != "" ) {
      $("#Hint").text('');
      $.ajax({
        url: 'php/action.php',
        type: 'POST',
        dataType: 'html',
        data:formData,
        cache:false,  //默认是true，但是一般不做缓存
        processData:false, //用于对data参数进行序列化处理，这里必须false；如果是true，就会将FormData转换为String类型
        contentType:false //一些文件上传http协议的关系，自行百度，如果上传的有文件，那么只能设置为false
      //   data: {act: 'say', nickName:nickName, trueName:trueName, towho:towho, email:email, contents:contents, gender:gender, itsGender:itsGender, image:Image}
      })
      
      .done(function(result) {
        console.log("success");
        //console.log(result);
        $("#hint").html("<a href='index.php' data-ajax='false'><img src='images/success.png'></a>");
        $("#nickName").val('');
        $("#trueName").val('');
        $("#towho").val('');
        $("#email").val('');
        $("#contents").val('');
        //$("#Hint").html(result);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(result) {
        console.log("complete");
      });
    } else {
      $("#hint").text("你应该写点东西哟").css('color', 'red').css('font-size', '16px');
    }
  });


});
function words_deal()
{
   var curLength=$("#contents").val().length;
   if(curLength>520)
   {
        var num=$("#contents").val().substr(0,520);
        $("contents").val(num);
   }
   else
   {
        $("#textCount").text(520-$("#contents").val().length);
   }
}

function words_cut(sentences, lengthCut) {
  if (sentences.length > lengthCut) {
    return sentences.substr(0,lengthCut);
  }else{
    return sentences;
  }
}


// 启用与关闭邮件通知
$("#switch").change(function(event) {
  console.log(event);
  console.log($(this));
});

$("#switch").click(function(event) {
  console.log($(this));
});

function openEmailNotice() {
  var status = $("#switch:checked").val();
  if (status == "on") {
    $("#email-box").css('display', 'block');
  }else{
    $("#email-box").css('display', 'none');
  }
}